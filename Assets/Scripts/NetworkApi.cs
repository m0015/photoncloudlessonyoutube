﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections;

public class NetworkApi : MonoBehaviour
{
    private static NetworkApi _instance;
    public static NetworkApi Instance { get { return _instance; } }

    public bool IsLoggedIn { get; private set; }
    // Use this for initialization
    void Start()
    {
        _instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public IEnumerator Login(string login)
    {
        var www = new WWW(string.Format("{0}?method=login&login={1}", Config.ServerUri, login));
        
        if (!www.isDone)
            yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogException(new Exception("NetworkApi.Login error:" + www.error));
            IsLoggedIn = true;
            yield break;
        }

        Debug.Log("result:" + www.text);

        var pars = www.text.Split(new[] {'&'}, StringSplitOptions.RemoveEmptyEntries);
        var dic = pars.Select(n => n.Split(new[] {'='}, StringSplitOptions.RemoveEmptyEntries))
            .ToDictionary(k => k[0], v => v[1]);

        Storage.PlayerHp = int.Parse(dic["hp"]);
        Storage.PlayerMoney = int.Parse(dic["money"]);
        Storage.PlayerPoints = int.Parse(dic["points"]);
        Storage.PlayerName = dic["name"];

        IsLoggedIn = true;
    }

    public IEnumerator UpdateInfo(string login, int points,int money, int hp)
    {
        var www = new WWW(string.Format("{0}?method=update&login={1}&hp={2}&money={3}&points={4}",
            Config.ServerUri,
            login,
            hp,
            money,
            points));

        if (!www.isDone)
            yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogException(new Exception("NetworkApi.Login error:" + www.error));
            yield break;
        }

        Debug.Log("NetworkApi.Update result: " + www.text);
    }
}
