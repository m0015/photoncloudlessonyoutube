<?php
include("config.php");

$db_conn = mysql_connect($db_host,$db_user,$db_pass);

if(!$db_conn){
	echo "db connection error!";
	exit;
}

$db_select = mysql_select_db($db_name,$db_conn);

if(!$db_select){
	echo "db selection error!";
	exit;
}

mysql_set_charset("UTF-8",$db_conn);
mysql_query("set names 'UTF8'");

class DB {
	public static function SendQuery ($query){
		return mysql_query ($query);
	}
	
	public static function GetFetchArray($query){
	  	return mysql_fetch_array (self::SendQuery($query));
	}
}
?>